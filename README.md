# Git Base

## Overview

This Git project base provides useful files that help to keep a clean and documented project.

## Installation

Copy the content of [`sources/`](./sources) folder into your project's root folder.

Add your [`LICENSE`](./sources/LICENSE) file's content to help users know your project's permission of use.

## Usage

You may want to override some files of [`sources/`](./sources) folder to match your needs.

## Details

The tree below represents this project's sources :

```
sources/
|-- .editorconfig
|-- .gitattributes
|-- .gitignore
|-- CHANGELOG.md
|-- LICENSE
'-- README.md
```

[`.editorconfig`](./sources/.editorconfig) - Defines the common configuration of code editors. *[Learn more](http://editorconfig.org/)*.

[`.gitattributes`](./sources/.gitattributes) - Defines which file types should be converted to `lf`. *[Learn more](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Attributes)*.

[`.gitignore`](./sources/.gitignore) - Define which files and folders should be ignored by Git. *[Learn more](https://git-scm.com/docs/gitignore)*.

[`CHANGELOG.md`](./sources/CHANGELOG.md) - Helps users to know your project's changes throughout revisions.

[`LICENSE`](./sources/LICENSE) - Helps users to know your project's permission of use.

[`README.md`](./sources/README.md) - Helps users to know how does the projects work, how to install it etc.

## [Change Log](./CHANGELOG.md)

## [License](./LICENSE)
