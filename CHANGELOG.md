# 1.0.0 (2016-02-09)

## Added

[`.editorconfig`](./.editorconfig) [`.gitattributes`](./.gitattributes) [`CHANGELOG.md`](./CHANGELOG.md) [`LICENSE`](./LICENSE) [`README.md`](./README.md) - This project's base.

[`sources/`](./sources/) - This project's sources.

[`sources/.editorconfig`](./sources/.editorconfig) [`sources/.gitattributes`](./sources/.gitattributes) [`sources/.gitignore`](./sources/.gitignore) [`sources/CHANGELOG.md`](./sources/CHANGELOG.md) [`sources/LICENSE`](./sources/LICENSE) [`sources/README.md`](./sources/README.md) - This project's source files.
